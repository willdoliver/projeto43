/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Willdoliver
 */
public class TrianguloEquilatero implements FiguraComLados {
    private final double lado;
    private final String nome = "TrianguloEquilatero";
    
    public TrianguloEquilatero (double lado){
        this.lado = lado;
    }
    
    @Override
    public double getLadoMaior(){
        return lado;
    }
    @Override
    public double getLadoMenor(){
        return lado;
    }
    
    @Override
    public double getArea(){
        return ((Math.pow(lado,2)*Math.sqrt(3))/4);
    }
    
    @Override
    public double getPerimetro(){
        return lado*3;
    }
    
    @Override
    public String getNome(){
        return nome;
    }
}
