/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Willdoliver
 */
public class Circulo extends Elipse {
        private final double raio;
        
        public Circulo(double raio){
            this.raio = raio;
        }
        
        @Override
        public double getArea(){
            return Math.PI * Math.pow(raio, 2);
        }
        
        @Override
        public double getPerimetro(){
            return 2*Math.PI*raio;
        }
    }
