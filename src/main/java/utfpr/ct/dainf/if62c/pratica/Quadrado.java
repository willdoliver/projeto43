/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author Willdoliver
 */
public class Quadrado implements FiguraComLados{
    private final double lado;
    private final String nome = "Quadrado";
    
    public Quadrado (double lado){
        this.lado = lado;
    }
    
    @Override
    public double getLadoMaior(){
        return lado;
    }
    @Override
    public double getLadoMenor(){
        return lado;
    }
    
    @Override
    public double getArea(){
        return Math.pow(lado, 2);
    }
    
    @Override
    public double getPerimetro(){
        return lado*4;
    }
    
    @Override
    public String getNome(){
        return nome;
    }
    
}
