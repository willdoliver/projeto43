/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Willdoliver
 */

import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

public class Projeto43 {
    public static void main(String[] args) {
        
        Retangulo retangulo1 = new Retangulo(10,5);
        Quadrado quadrado1 = new Quadrado(10);
        TrianguloEquilatero triangulo1 = new TrianguloEquilatero(10);
        
        
        System.out.println("Area Retangulo: " + retangulo1.getArea());
        System.out.println("Perimetro Retangulo: " + retangulo1.getPerimetro());
        
        System.out.println("Area Quadrado: " + quadrado1.getArea());
        System.out.println("Perimetro Quadrado: " + quadrado1.getPerimetro());
        
        System.out.println("Area Triangulo Equilatero: " + triangulo1.getArea());
        System.out.println("Perimetro Triangulo Equilatero: " + triangulo1.getPerimetro());
    }
}
